import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home,
    meta: {
      title: "EVEKill.club | Home"
    }
  },
  {
    path: "/about",
    name: "about",
    meta: {
      title: "EVEKill.club | About"
    },
    component: () =>
      import("../views/About.vue")
  },
  {
    path: "/kill/:killID",
    props: true,
    name: "kill",
    meta: {
      title: "EVEKill.club | Kill"
    },
    component: () => import("../views/Kill.vue")
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
