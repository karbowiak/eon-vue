import Vue from "vue";
import axios from "axios";
import VueAxios from "vue-axios";
import underscore from 'vue-underscore';
import App from "./App.vue";
import router from "./router";
import store from "./store";
import { library } from '@fortawesome/fontawesome-svg-core';
import { faDotCircle } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

library.add(faDotCircle);
Vue.component('fa', FontAwesomeIcon); // Use like: <div id="app"><fa icon="user-secret"/></div>

Vue.config.productionTip = false;

// Modules
Vue.use(require("vue-moment"));
Vue.use(VueAxios, axios);
Vue.use(underscore);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
